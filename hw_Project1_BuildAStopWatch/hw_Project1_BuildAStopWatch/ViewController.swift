//
//  ViewController.swift
//  hw_Project1_BuildAStopWatch
//
//  Created by Gilbert Hopkins on 5/13/19.
//  Copyright © 2019 Hop. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var startStopLabel: UIButton!
    @IBOutlet weak var resumeLabel: UIButton!
    @IBOutlet weak var resetLabel: UIButton!
    
    var isStart : Bool = true;
    var count = 0;
    var timer : Timer?;
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


    @IBAction func startStopButton(_ sender: UIButton) {
        if(isStart == true){
            self.isStart = false;
            self.startStopLabel.setTitle("Stop", for: .normal)
            self.startTimer();
        } else {
            self.timer?.invalidate(); // stoping repeated loop
            self.startStopLabel.isHidden = true;
            self.resumeLabel.isHidden = false;
            self.resetLabel.isHidden = false;
        }
    }

    @IBAction func resumeButton(_ sender: UIButton) {
        self.startTimer();
        self.startStopLabel.isHidden = false;
        self.resumeLabel.isHidden = true;
        self.resetLabel.isHidden = true;
    }
    
    @IBAction func resetButton(_ sender: UIButton) {
        self.startStopLabel.setTitle("Start", for: .normal);
        self.startStopLabel.isHidden = false;
        self.resumeLabel.isHidden = true;
        self.resetLabel.isHidden = true;
        self.count = 0;
        self.isStart = true;
        self.timerLabel.text = "00";
        
    }
    
    func startTimer() {
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(counter), userInfo: nil, repeats: true)
    }
    
    @objc func counter() {
        self.count += 1;
        if (self.count < 10){
            timerLabel.text = "0\(String(self.count))";
        } else {
            timerLabel.text = "\(String(self.count))";
        }
    }
    
}

